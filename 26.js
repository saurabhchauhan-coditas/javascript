let fruits= ["apple","mango","grapes"];
// let numbers= [1,2,3,4];
// let mixed= [1,2,2.3,'string', null, undefined];
// console.log(mixed);
// console.log(numbers);
// console.log(fruits[2]);

// fruits[1] = "banana";
// console.log(fruits);

// console.log(typeof mixed);

// Arrays.isArray(fruits);

//push

fruits.push("banana");

// let poppedFruit= fruits.pop();

// fruits.unshift("banana"); // adds element at starting
// console.log(fruits);

let removedFruit = fruits.shift();  // removes first element and returns it 
console.log(fruits);