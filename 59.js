// reduce

const numbers = [1,2,3,4,5, 10];

// aim: sum of all the numbers in array

const sum = numbers.reduce((accumulator, currentValue)=>{
    return accumulator + currentValue;
});

// constructor(params){
//}

const userCart = [
{productId: 1, productName: "mobile", price: 12000},
{productId: 2, productName: "laptop", price: 22000},
{productId: 3, productName: "tv", price: 15000}
]

const totalAmount = userCart.reduce((totalPrice, currentProduce)=>{
return totalPrice+currentProduce.price;}
);

console.log(totalAmount);