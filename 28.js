// primitive data types-- gets stored in stack because they use up much space

let num1=6;
let num2=num1;

console.log(num1);
console.log(num2);

num1++;

console.log(num1);
console.log(num2); // num2 doestn change

// reference data types-- gets stored in heap memory

let array1= ["item1","item2"];
let array2=array1;

console.log(array1);
console.log(array2);

array1.push("item3");

console.log(array1);
console.log(array2); // array2 also gets change, both gets same address of pointers in stack which references to heap memory

