// static methods and properties

class Person{ 
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age= age;
    }
    static classInfo(){
        return 'this is person class'
    }

    get fullName(){
    return
        `${this.firstName} ${this.lastName}`
    }
    
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName = lastName;
    }

    eat(){
        return `${this.name} is eating`;
    }

    isSuperCute(){
        return this.age <=1;
    }

    isCute(){
        return true;
    }
}

const person1 = new Person("saurabh","chauhan",8);

// person1.classInfo();

const ans= Person.classInfo();
console.log(ans);