// sort
// 5,9,1200, 400, 3000 // 5, 9, 400, 1200, 3000 (expected)

const numbers = [5,9,1200, 410, 3000]; // 
// ["5", "9", "1210", "410", "3000"] // [53, 57, 49, 52, 51]

const userNames = ['harshit', 'abcd', 'mohit', 'nitish']
userNames.sort();
console.log(userNames);

numbers.sort((a,b)=>{
    return a-b;
});
console.log(numbers);

const products = [
    {productId: 1, produceName: "pl", price: 300 },
    {productId: 2, produceName: "p2", price: 300 },
    {productId: 3, produceName: "p3", price: 300 },
    {productId: 1, produceName: "pl", price: 300 },
    {productId: 1, produceName: "pl", price: 300 },
]
const highToLow = products.slice(0).sort((a, b)=>{ return b.price-a.price;
});
console.log(highToLow);